package main

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/pokt-network/pocket-core/app"
	"github.com/pokt-network/pocket-core/app/cmd/rpc"
	"github.com/pokt-network/pocket-core/crypto"
	"github.com/pokt-network/pocket-core/crypto/keys/mintkey"
	sdk "github.com/pokt-network/pocket-core/types"
	appTypes "github.com/pokt-network/pocket-core/x/apps/types"
	"github.com/pokt-network/pocket-core/x/auth"
	authTypes "github.com/pokt-network/pocket-core/x/auth/types"
	govTypes "github.com/pokt-network/pocket-core/x/gov/types"
	nodeTypes "github.com/pokt-network/pocket-core/x/nodes/types"
	"github.com/pokt-network/pocket-core/x/pocketcore/types"
	pcTypes "github.com/pokt-network/pocket-core/x/pocketcore/types"
)

func EncodeTx(msg sdk.ProtoMsg, networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (txBz []byte, errRet error) {
	var er sdk.Error
	var err error
	er = msg.ValidateBasic()
	if er != nil {
		errRet = errors.New("Message validation failed: " + er.Error())
		return
	}

	fee, ok := sdk.NewIntFromString(feeStr)
	if !ok {
		errRet = errors.New("Invalid fee: " + feeStr)
		return
	}
	fees := sdk.NewCoins(sdk.NewCoin(sdk.DefaultStakeDenom, fee))

	entropy, err := strconv.ParseInt(entropyStr, 10, 64)
	if err != nil {
		errRet = errors.New("Invalid entropy: " + entropyStr)
		return
	}

	signBytes, err := auth.StdSignBytes(networkID, entropy, fees, msg, memo)
	if err != nil {
		errRet = errors.New("Cannot build StdSignBytes: " + err.Error())
		return
	}

	sig, err := privKey.Sign(signBytes)
	if err != nil {
		errRet = errors.New("Cannot sign data: " + err.Error())
		return
	}

	s := authTypes.StdSignature{
		PublicKey: privKey.PublicKey(),
		Signature: sig,
	}

	tx := authTypes.NewTx(msg, fees, s, memo, entropy)
	txBz, err = auth.DefaultTxEncoder(app.Codec())(tx, -1)
	if err != nil {
		errRet = errors.New("Cannot encode data: " + err.Error())
		return
	}

	return txBz, nil
}

func CreateSendRawTxParams(
	fromAddr, toAddr, amountStr, networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (param *rpc.SendRawTxParams, errRet error) {
	fa, err := sdk.AddressFromHex(fromAddr)
	if err != nil {
		errRet = errors.New("Invalid fromAddr: " + fromAddr)
		return
	}
	ta, err := sdk.AddressFromHex(toAddr)
	if err != nil {
		errRet = errors.New("Invalid toAddr: " + toAddr)
		return
	}
	if fa.Equals(ta) {
		errRet = errors.New("Recipient is the same as sender")
		return
	}

	amount, ok := sdk.NewIntFromString(amountStr)
	if !ok || amount.LTE(sdk.ZeroInt()) {
		errRet = errors.New("Invalid amount: " + amountStr)
		return
	}
	msg := nodeTypes.MsgSend{
		FromAddress: fa,
		ToAddress:   ta,
		Amount:      amount,
	}
	txBz, errRet := EncodeTx(&msg, networkID, feeStr, memo, entropyStr, privKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        fromAddr,
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func CreateStakeTxParams(
	operatorPubkeyStr, outputAddrStr, amountStr, chainsStr, url, delegatorsStr,
	networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (param *rpc.SendRawTxParams, errRet error) {
	operatorPubkey, err := crypto.NewPublicKey(operatorPubkeyStr)
	if err != nil {
		errRet = errors.New("Cannot encode the operator: " + err.Error())
		return
	}

	chains := strings.Split(chainsStr, ",")

	amount, ok := sdk.NewIntFromString(amountStr)
	if !ok || amount.LTE(sdk.ZeroInt()) {
		errRet = errors.New("Invalid amount: " + amountStr)
		return
	}

	outputAddress, err := sdk.AddressFromHex(outputAddrStr)
	if err != nil {
		errRet = errors.New("Cannot encode the output address: " + err.Error())
		return
	}

	var delegators map[string]uint32
	if len(delegatorsStr) > 0 {
		if err = json.Unmarshal([]byte(delegatorsStr), &delegators); err != nil {
			errRet = errors.New("Cannot unmarsal the delegators: " + err.Error())
			return
		}
	}

	fromAddr := sdk.Address(privKey.PublicKey().Address())

	msg := nodeTypes.MsgStake{
		PublicKey:        operatorPubkey,
		Chains:           chains,
		Value:            amount,
		ServiceUrl:       url,
		Output:           outputAddress,
		RewardDelegators: delegators,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, privKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        fromAddr.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func CreateUnstakeTxParams(
	operatorAddrStr,
	networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (param *rpc.SendRawTxParams, errRet error) {
	operatorAddr, err := sdk.AddressFromHex(operatorAddrStr)
	if err != nil {
		errRet = errors.New("Cannot encode the output address: " + err.Error())
		return nil, err
	}

	fromAddr := sdk.Address(privKey.PublicKey().Address())
	msg := nodeTypes.MsgBeginUnstake{
		Address: operatorAddr,
		Signer:  fromAddr,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, privKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        fromAddr.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func CreateUnjailTxParams(
	operatorAddrStr,
	networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (param *rpc.SendRawTxParams, errRet error) {
	operatorAddr, err := sdk.AddressFromHex(operatorAddrStr)
	if err != nil {
		errRet = errors.New("Cannot encode the output address: " + err.Error())
		return nil, err
	}

	fromAddr := sdk.Address(privKey.PublicKey().Address())
	msg := nodeTypes.MsgUnjail{
		ValidatorAddr: operatorAddr,
		Signer:        fromAddr,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, privKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        fromAddr.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func createProofs(sbh, totalProofs int64,
	chainId, servicerPubKey string,
	appPrivKey crypto.PrivateKey) (
	proofs []pcTypes.Proof, errRet error) {
	appPubKey := appPrivKey.PublicKey().RawString()

	aat := pcTypes.AAT{
		Version:              "0.0.1",
		ApplicationPublicKey: appPubKey,
		ClientPublicKey:      appPubKey,
		ApplicationSignature: "",
	}
	aatSig, err := appPrivKey.Sign(aat.Hash())
	if err != nil {
		errRet = errors.New("AAT signature failure: " + err.Error())
		return
	}
	aat.ApplicationSignature = hex.EncodeToString(aatSig)

	proofs = make([]pcTypes.Proof, totalProofs)
	for i := int64(0); i < totalProofs; i++ {
		relay := types.Relay{
			Payload: pcTypes.Payload{
				Data:    ":)",
				Method:  "",
				Path:    "",
				Headers: map[string]string{},
			},
			Meta: pcTypes.RelayMeta{BlockHeight: sbh},
			Proof: pcTypes.RelayProof{
				Entropy:            i + 1,
				SessionBlockHeight: sbh,
				ServicerPubKey:     servicerPubKey,
				Blockchain:         chainId,
				Token:              aat,
				Signature:          "",
			},
		}

		relay.Proof.RequestHash = relay.RequestHashString()

		appSig, err := appPrivKey.Sign(relay.Proof.Token.Hash())
		if err != nil {
			errRet = errors.New("Application signature failure: " + err.Error())
			return
		}
		relay.Proof.Token.ApplicationSignature = hex.EncodeToString(appSig)

		clientSig, err := appPrivKey.Sign(relay.Proof.Hash())
		if err != nil {
			errRet = errors.New("Client signature failure: " + err.Error())
			return
		}
		relay.Proof.Signature = hex.EncodeToString(clientSig)

		proofs[i] = relay.Proof
	}

	return proofs, nil
}

func CreateClaimTxParams(
	sbhStr, chainId, totalProofsStr,
	networkID, feeStr, memo, entropyStr string,
	appPrivKey, signerPrivKey crypto.PrivateKey) (
	param *rpc.SendRawTxParams, errRet error) {
	sbh, err := strconv.ParseInt(sbhStr, 10, 64)
	if err != nil {
		errRet = errors.New("Invalid SBH: " + sbhStr)
		return
	}

	totalProofs, err := strconv.ParseInt(totalProofsStr, 10, 64)
	if err != nil {
		errRet = errors.New("Invalid TotalProofs: " + totalProofsStr)
		return
	}

	servicerPubKey := signerPrivKey.PublicKey()
	servicerAddr := sdk.Address(servicerPubKey.Address())

	proofs, err := createProofs(
		sbh, totalProofs, chainId, servicerPubKey.RawString(), appPrivKey)
	if err != nil {
		errRet = err
		return
	}

	appPubKey := appPrivKey.PublicKey().RawString()

	root, _ := pcTypes.GenerateRoot(sbh, proofs)

	msg := pcTypes.MsgClaim{
		SessionHeader: pcTypes.SessionHeader{
			SessionBlockHeight: sbh,
			Chain:              chainId,
			ApplicationPubKey:  appPubKey,
		},
		MerkleRoot:       root,
		TotalProofs:      totalProofs,
		FromAddress:      servicerAddr,
		EvidenceType:     pcTypes.RelayEvidence,
		ExpirationHeight: 0,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, signerPrivKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        servicerAddr.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

type pseudorandomGenerator struct {
	BlockHash string
	Header    string
}

func getPseudorandomIndex(proofBlockHashStr string,
	totalRelays int64, header pcTypes.SessionHeader) (int64, error) {
	pseudoGenerator :=
		pseudorandomGenerator{proofBlockHashStr, header.HashString()}
	r, err := json.Marshal(pseudoGenerator)
	if err != nil {
		return 0, err
	}
	return pcTypes.PseudorandomSelection(
		sdk.NewInt(totalRelays), pcTypes.Hash(r)).Int64(), nil
}

func CreateProofTxParams(
	sbhStr, chainId, totalProofsStr, proofBlockHashStr,
	networkID, feeStr, memo, entropyStr string,
	appPrivKey, signerPrivKey crypto.PrivateKey) (
	param *rpc.SendRawTxParams, errRet error) {
	sbh, err := strconv.ParseInt(sbhStr, 10, 64)
	if err != nil {
		errRet = errors.New("Invalid SBH: " + sbhStr)
		return
	}

	totalProofs, err := strconv.ParseInt(totalProofsStr, 10, 64)
	if err != nil {
		errRet = errors.New("Invalid TotalProofs: " + totalProofsStr)
		return
	}

	servicerPubKey := signerPrivKey.PublicKey()

	proofs, err := createProofs(
		sbh, totalProofs, chainId, servicerPubKey.RawString(), appPrivKey)
	if err != nil {
		errRet = err
		return
	}

	signer := sdk.Address(signerPrivKey.PublicKey().Address())
	appPubKey := appPrivKey.PublicKey().RawString()

	evidence := pcTypes.Evidence{
		SessionHeader: pcTypes.SessionHeader{
			SessionBlockHeight: sbh,
			Chain:              chainId,
			ApplicationPubKey:  appPubKey,
		},
		NumOfProofs: totalProofs,
		Proofs:      proofs,
	}

	index, err := getPseudorandomIndex(
		proofBlockHashStr, totalProofs, evidence.SessionHeader)
	if err != nil {
		errRet = err
		return
	}

	mProof, leaf := evidence.GenerateMerkleProof(sbh, int(index), totalProofs)

	msg := pcTypes.MsgProof{
		MerkleProof:  mProof,
		Leaf:         leaf,
		EvidenceType: pcTypes.RelayEvidence,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, signerPrivKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        signer.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func CreateGovChangeTxParams(
	key, value, networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (param *rpc.SendRawTxParams, errRet error) {
	fromAddr := sdk.Address(privKey.PublicKey().Address())

	valueBytes, err := app.Codec().MarshalJSON(value)
	if err != nil {
		errRet = errors.New("Invalid value: " + value)
		return
	}

	msg := govTypes.MsgChangeParam{
		FromAddress: fromAddr,
		ParamKey:    key,
		ParamVal:    valueBytes,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, privKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        fromAddr.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func CreateAppStakeTxParams(
	appPubkeyStr, amountStr, chainsStr,
	networkID, feeStr, memo, entropyStr string,
	privKey crypto.PrivateKey) (param *rpc.SendRawTxParams, errRet error) {
	appPubkey, err := crypto.NewPublicKey(appPubkeyStr)
	if err != nil {
		errRet = errors.New("Cannot encode the public key: " + err.Error())
		return
	}

	amount, ok := sdk.NewIntFromString(amountStr)
	if !ok || amount.LTE(sdk.ZeroInt()) {
		errRet = errors.New("Invalid amount: " + amountStr)
		return
	}

	chains := strings.Split(chainsStr, ",")

	fromAddr := sdk.Address(privKey.PublicKey().Address())

	msg := appTypes.MsgStake{
		PubKey: appPubkey,
		Chains: chains,
		Value:  amount,
	}
	txBz, errRet := EncodeTx(
		&msg, networkID, feeStr, memo, entropyStr, privKey)
	if errRet != nil {
		return
	}

	return &rpc.SendRawTxParams{
		Addr:        fromAddr.String(),
		RawHexBytes: hex.EncodeToString(txBz),
	}, nil
}

func LoadArmoredJson(
	jsonOrFilePath, passphrase string) (crypto.PrivateKey, error) {
	privKey, err := mintkey.UnarmorDecryptPrivKey(jsonOrFilePath, passphrase)
	if err == nil {
		return privKey, nil
	}

	jsonBytes, err := os.ReadFile(jsonOrFilePath)
	if err != nil {
		return nil, err
	}
	return mintkey.UnarmorDecryptPrivKey(string(jsonBytes), passphrase)
}

func main() {
	if len(os.Args) < 2 {
		fmt.Print(`USAGE:
  pocket-tx-builder create <passphrase>
  pocket-tx-builder decode <armoredJson> <passphrase>
  pocket-tx-builder send <fromAddr> <toAddr> <amount> <networkID> <fee> <memo> <entropy> <armoredJson> <passphrase>
  pocket-tx-builder stake <opereatorPubKey> <outputAddr> <amount> <chains> <url> <delegators> <networkID> <fee> <memo> <entropy> <armoredJson> <passphrase>
  pocket-tx-builder unstake <opereatorAddr> <networkID> <fee> <memo> <entropy> <armoredJson> <passphrase>
  pocket-tx-builder unjail <opereatorAddr> <networkID> <fee> <memo> <entropy> <armoredJson> <passphrase>
  pocket-tx-builder claim <sbh> <chainId> <totalProofs> <ArmoredJson of App> <passphrase> <networkID> <fee> <memo> <entropy> <ArmoredJson of Node> <passphrase>
  pocket-tx-builder proof <sbh> <chainId> <totalProofs> <proofBlockHash> <ArmoredJson of App> <passphrase> <networkID> <fee> <memo> <entropy> <ArmoredJson of Node> <passphrase>
  pocket-tx-builder change <key> <value> <networkID> <fee> <memo> <entropy> <armoredJson> <passphrase>
  pocket-tx-builder appstake <appPubkey> <amount> <chains> <networkID> <fee> <memo> <entropy> <armoredJson> <passphrase>
`)
		os.Exit(1)
	}

	armoredJsonIndex := -1

	cmd := os.Args[1]
	if cmd == "create" {
		armoredJsonIndex = 1
	} else if cmd == "decode" {
		armoredJsonIndex = 2
	} else if cmd == "send" || cmd == "appstake" {
		armoredJsonIndex = 9
	} else if cmd == "unstake" || cmd == "unjail" {
		armoredJsonIndex = 7
	} else if cmd == "claim" {
		armoredJsonIndex = 11
	} else if cmd == "stake" || cmd == "proof" {
		armoredJsonIndex = 12
	} else if cmd == "change" {
		armoredJsonIndex = 8
	} else {
		fmt.Println("Not supported:", cmd)
		os.Exit(1)
	}

	if len(os.Args) < armoredJsonIndex+2 {
		fmt.Println("Inssuficient args")
		os.Exit(1)
	}

	armoredJson := os.Args[armoredJsonIndex]
	passphrase := os.Args[armoredJsonIndex+1]

	if cmd == "create" {
		privKey := crypto.PrivateKey(crypto.Ed25519PrivateKey{}).GenPrivateKey()
		armoredJson, err := mintkey.EncryptArmorPrivKey(privKey, passphrase, "")
		if err != nil {
			fmt.Println("Cannot encrypt the private key:", err.Error())
			return
		}
		fmt.Print(armoredJson)
		return
	}

	privKey, err := LoadArmoredJson(armoredJson, passphrase)
	if err != nil {
		fmt.Println("Cannot decrypt the private key:", err.Error())
		return
	}

	if cmd == "decode" {
		pubkey := privKey.PublicKey()
		fmt.Printf("%s%s",
			strings.ToLower(pubkey.Address().String()),
			privKey.RawString(), // <privkey32b><pubkey32b>
		)
		return
	}

	var param *rpc.SendRawTxParams
	if cmd == "send" {
		param, err = CreateSendRawTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[6],
			os.Args[7],
			os.Args[8],
			privKey,
		)
	} else if cmd == "stake" {
		param, err = CreateStakeTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[6],
			os.Args[7],
			os.Args[8],
			os.Args[9],
			os.Args[10],
			os.Args[11],
			privKey,
		)
	} else if cmd == "unstake" {
		param, err = CreateUnstakeTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[6],
			privKey,
		)
	} else if cmd == "unjail" {
		param, err = CreateUnjailTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[6],
			privKey,
		)
	} else if cmd == "claim" {
		appPrivKey, err := LoadArmoredJson(os.Args[5], os.Args[6])
		if err != nil {
			fmt.Println("Cannot decrypt the private key:", err.Error())
			return
		}
		param, err = CreateClaimTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[7],
			os.Args[8],
			os.Args[9],
			os.Args[10],
			appPrivKey,
			privKey,
		)
	} else if cmd == "proof" {
		appPrivKey, err := LoadArmoredJson(os.Args[6], os.Args[7])
		if err != nil {
			fmt.Println("Cannot decrypt the private key:", err.Error())
			return
		}
		param, err = CreateProofTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[8],
			os.Args[9],
			os.Args[10],
			os.Args[11],
			appPrivKey,
			privKey,
		)
	} else if cmd == "change" {
		param, err = CreateGovChangeTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[6],
			os.Args[7],
			privKey,
		)
	} else if cmd == "appstake" {
		param, err = CreateAppStakeTxParams(
			os.Args[2],
			os.Args[3],
			os.Args[4],
			os.Args[5],
			os.Args[6],
			os.Args[7],
			os.Args[8],
			privKey,
		)
	}

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	paramStr, err := json.Marshal(param)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(string(paramStr))
}
